<html>
    <head>
        <title>About us</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="About us-style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <nav>
            <ul>
                <li><a href="home.php">Home</a></li>
                <li><a href="Five types of fitness training.php">Flexibility Training</a>
                    <ul>
                        <li><a href="Dynamic.php">Dynamic Strength-training</a></li>
                        <li><a href="Static.php">Static Strength-training</a></li>
                        <li><a href="Aerobic.php">Aerobic Training</a></li>
                        <li><a href="Circuit.php">Circuit Training</a></li>
                    
            </ul>
        </li>
        <li><a href="About us.php">About us</a></li>
        <li><a href="Contact us.php">Contact us</a></li>
    </ul>
</nav>
<div>
    <main>
        <section>
            <h1 style="font-size:200%;font-family:impact;color:crimson;font-style: oblique">Health Info</h1>
        </section>
        <p>
            Health is the level of functional or metabolic efficiency of a living organism. In humans, it is the general condition of a person's mind and body, usually meaning to be free from illness, injury or pain (as in "good health" or "healthy").
            The World Health Organization (WHO) defined health in its broader sense in 1946 as "a state of complete physical, mental, and social well-being and not merely the absence of disease or infirmity."
            Although this definition has been subject to controversy, in particular as lacking operational value and because of the problem created by use of the word "complete," it remains the most enduring.[4][5] Other definitions have been proposed, among which a recent definition that correlates health and personal satisfaction.
            Classification systems such as the WHO Family of International Classifications, including the International Classification of Functioning, Disability and Health (ICF) and the International Classification of Diseases (ICD), are commonly used to define and measure the components of health.
        </p>
        <img src="Obese_population_OECD_2010.png" style="width:600px;length:600px"><img src="Overweight_or_obese_population_OECD_2010.png" style="width:600px;length:600px">
        <section>
            <h2>Fitness may refer to:</h2>
        </section>
        <p>
            Physical fitness, a general state of good health, usually as a result of exercise and nutrition<br>
            Fitness (biology), an individual's ability to propagate its genes<br>
            Fitness (magazine), a women's magazine, focusing on health and exercise<br>
            Fitness and figure competition, a form of physique training, related to bodybuilding<br>
            Fitness approximation, a method of function optimization evolutionary computation or artificial evolution methodologies<br>
            Fitness function, a particular type of objective function in mathematics and computer science<br>
        </p>
        <section>
            <h3>Preparation</h3>
        </section>
        <p>
            Achieving and maintaining health is an ongoing process, shaped by both the evolution of health care knowledge and practices as well as personal strategies and organized interventions for staying healthy known as Lifestyle Management.
        <ul>
            <li style="font-size:200%;font-family:impact;color:crimson;font-style: oblique">Diet</li>
            <p>
                An important way to maintain your personal health is to have a healthy diet. A healthy diet includes a variety of plant-based and animal-based foods that provide nutrients to your body. Such nutrients give you energy and keep your body running. Nutrients help build and strengthen bones, muscles, and tendons and also regulate body processes (i.e. blood pressure). 
                The food guide pyramid is a pyramid-shaped guide of healthy foods divided into sections. Each section shows the recommended intake for each food group (i.e. Protein, Fat, Carbohydrates, and Sugars). Making healthy food choices is important because it can lower your risk of heart disease, developing some types of cancer, and it will contribute to maintaining a healthy weight.
            </p>
            <li style="font-size:200%;font-family:impact;color:crimson;font-style: oblique">Exercise</li>
            <p>
                Physical exercise enhances or maintains physical fitness and overall health and wellness. It strengthens muscles and improves the cardiovascular system.
            </p>
            <li style="font-size:200%;font-family:impact;color:crimson;font-style: oblique">Role of science</li>
            <p>
                Health science is the branch of science focused on health. There are two main approaches to health science: the study and research of the body and health-related issues to understand how humans (and animals) function, and the application of that knowledge to improve health and to prevent and cure diseases and other physical and mental impairments. 
                The science builds on many sub-fields, including biology, biochemistry, physics, epidemiology, pharmacology, medical sociology. 
                Applied health sciences endeavor to better understand and improve human health through applications in areas such as health education, biomedical engineering, biotechnology and public health.
                Organized interventions to improve health based on the principles and procedures developed through the health sciences are provided by practitioners trained in medicine, nursing, nutrition, pharmacy, social work, psychology, occupational therapy, physical therapy and other health care professions. 
                Clinical practitioners focus mainly on the health of individuals, while public health practitioners consider the overall health of communities and populations. Workplace wellness programs are increasingly adopted by companies for their value in improving the health and well-being of their employees, as are school health services in order to improve the health and well-being of children.
            </p>
        </ul>
        </p>
    </main>
</div>
</body>
</html>
