<?php
    $host = "localhost";
    $user = "root";
    $password = "";
    $database = "cartoon";
    $link = mysqli_connect($host, $user, $password, $database) or die("error connecting to DB" . mysqli_errno($link));
//
//    $query = "SELECT * FROM feedback";
//   
//    $result = mysqli_query($link, $query);
    ?>




<html>
    <head>
        <title>Contact us</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="Contact-style.css" rel="stylesheet" type="text/css"/>
        <style>
            img {
                opacity: 0.4;
                filter: alpha(opacity=40); /* For IE8 and earlier */
            }

            img:hover {
                opacity: 1.0;
                filter: alpha(opacity=100); /* For IE8 and earlier */
            }
        </style>
        <script src="validate.js" type="text/javascript"></script>
    </head>
    <body>
        <nav>
            <ul>
                <li><a href="home.php">Home</a></li>
                <li><a href="Five types of fitness training.php">Flexibility Training</a>
                    <ul>
                        <li><a href="Dynamic.php">Dynamic Strength-training</a></li>
                        <li><a href="Static.php">Static Strength-training</a></li>
                        <li><a href="Aerobic.php">Aerobic Training</a></li>
                        <li><a href="Circuit.php">Circuit Training</a></li>

                    </ul>
                </li>
                <li><a href="About us.php">About us</a></li>
                <li><a href="Contact us.php">Contact us</a></li>
            </ul>
        </nav>
        <div>
                <form name="theForm" id="theForm" action="readLocalStorage.php" onsubmit=" return validateData()" method="POST" enctype="text/plain">           
                <span class="required">*</span><label>Name: </label><input name="name" id="name" size="20" placeholder="at least 2 characters"  autofocus required /><span class="error" id="showErrorMessage1"></span>
                <br><br>

                <span class="required">*</span><label>Address </label><input name="address" id="address" size="30" placeholder="at least 10 characters"  required /> <span class="error" id="showErrorMessage2"></span>
                <br><br>

                <label>Age: </label>
                <input type="number" name="age" id="age" min="7" max="40" placeholder="7-40" /><br>
                <br><br>
                <label>E-mail: </label><input type="email" name="email" id="email" /><span id="showErrorMessage4"></span>
                <br><br>
                <label>Mobile:</label><input type="text" name="mobile" id="mobile"><span id="showErrorMessage5"></span>
                <br><br>
                <label>Date: </label><input type="date" name="date" id="dateNum">
                <br><br>
                Comment:<textarea type="text" id="comment" name="comment" placeholder="Write down your comment"></textarea><span id="showErrorMessage6"></span>
                <br><br><br>


                <label>Type: </label> 
                <select id="type" name="type" required multiple>
                    <option selected >Please select your favorite type...</option>
                    <option>China</option>
                    <option>Japan</option>
                    <option>America</option>
                    <option>Korea</option>
                </select><br><br>
                
                
                
                <label>How would you rate this web-site[1,10]?</label>
                <input type="range" name="rate" min="1" max="10" value="5"><br><br><br>


                <label></label><input type="submit" value="Click Here to Submit" />
                <br><br>

                <br><br>
                <label></label><input type="reset" value="Reset the Form (Note: different from reload webpage)" />

            </form>
            <audio controls>
                <source src="AlexBeroza - The New Music.ogg" type="audio/ogg">
                <source src="AlexBeroza - The New Music.mp3" type="audio/mpeg">
                Your browser does not support the audio element.
            </audio>
            <main>
                <section>
                    <h1 style="font-size:200%;font-family:Helvetica;  font-style:initial;color:orange">Here are the 4 best ways to contact</h2>
                </section>

                <p>
                <ol>
                    <li style="font-size:150%;font-family:impact;color:crimson;font-style: oblique">Email</li>
                    <img src="logo.png">
                    <li style="font-size:150%;font-family:impact;color:crimson;font-style: oblique">Direct Mail</li>
                    <img src="background-fitness.jpg">
                    <li style="font-size:150%;font-family:impact;color:crimson;font-style: oblique">SMS Text/Mobile</li>
                    <img src="Never say never.jpg">
                    <li style="font-size:150%;font-family:impact;color:crimson;font-style: oblique">Social Media</li>
                </ol>
                </p>                                       
            </main>
        </div>
    </body>
</html>
