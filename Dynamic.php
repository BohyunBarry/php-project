<html>
    <head>
        <title>Dynamic Strength</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="Dynamic-style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <nav>
            <ul>
                <li><a href="home.php">Home</a></li>
                <li><a href="Five types of fitness training.php">Five types of fitness training-Flexibility Training</a>
                    <ul>
                        <li><a href="Dynamic.php">Dynamic Strength-training</a></li>
                        <li><a href="Static.php">Static Strength-training</a> </li>
                        <li><a href="Aerobic.php">Aerobic Training</a></li>
                        <li><a href="Circuit.php">Circuit Training</a></li>
                    
            </ul>
        </li>
        <li><a href="About us.php">About us</a></li>
        <li><a href="Contact us.php">Contact us</a></li>
    </ul>
</nav>
<div>
    <main>
        <section>
            <h1 style="font-size:200%;font-family:monospace;color:red">Dynamic Strength-training</h1>
        </section>
        <p>
            Dynamic strength training is considered an anaerobic exercise, and is also known as isotonic exercise. This type of exercise strengthens your muscles over a full-range of motion. 
            Weightlifting and calisthenics are examples of dynamic strength-training. This type of exercise uses resistance to work your muscles through a completed motion, such as performing a bench press, leg press or situp.
        </p>
        <section>
            <h2>Ways to increase your strength:</h2>
        </section>
        <p>
        <ol>
            <li style="font-size:150%">Bodyweight exercises like push-ups or squats, that require nothing but your body.</li>
            <li style="font-size:150%">Free-weights, which requires some equipment.</li>
            <li style="font-size:150%">Cardio work, such as running, to improve endurance.</li>
            <li style="font-size:150%">Gym machines, which need a expensive equipment and are not as effective as bodyweight or free-weights equivalents.</li>
        </ol>
        </p>
        <section>
            <h3>Bodyweight Training</h3>
        </section>
        <p>
            Bodyweight exercises are extremely convenient and effective. You don’t need any equipment or gym subscription, since you’ll be using your own body as your resistance.
            Because they’re so simple, it’s easy to prevent injuries. If you’re new to strength training and still need some help with form on your exercises, bodyweight training is much more forgiving than most types of weightlifting.
            Bodyweight exercises are also incredibly easy to modify to your ability level, whether you’re a recovering couch potato or world-class gymnast. They also allow you to develop better control and awareness.
        </p>
        <section>
            <h4>Free Weights</h4>
        </section>
        <p>
            Free weight exercises are those done with dumbbells and barbells. Because they work your muscles and stabilizers, they’re great at building a strong, balanced physique.
            These also help improve bone density, tendon strength and strengthen your metabolism.
            Begin to see everyday movements as strength training opportunities. Every time you pick something up off the ground, you’re doing a mini-deadlift.
            Specific exercises with loaded weights are ideal to strengthen muscular imbalances since it’s easier to target individual muscle groups; for this type of focus, free weights can be very efficient exercises.
            If total-body workouts are more difficult for one area of your body, strengthen it with specific weighted exercises.
        </p>
        <section>
            <h5>Weight Machines</h5>
        </section>
        <p>
            Machines provide a specific range of motion and restrict your movements to avoid bad form. Because of this, they can be great for rehabilitation exercises. They’re also good for beginners.
            If you’re just getting into weightlifting, machines are a good place to start.
            Machines can’t deliver the kind of full-body results you can get with bodyweight or free-weight exercises. Machines will typically isolate one muscle group, which is potentially dangerous as your strength builds up.
        </p>
        <section>
            <h6>Dynamic Strength Training</h6>
        </section>
        <p>
            Dynamic strength training is what happens in HIIT workouts, usually involving plyometrics, or quick, explosive motion.
            These types of exercises engage your muscles explosively, at fairly light loads, effectively combining some plyometric concepts with the benefits of lifting weights.
        </p>
    </main>
</div>
</body>
</html>
