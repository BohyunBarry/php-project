<html>
    <head>
        <title>Static Training</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="static-style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <nav>
            <ul>
                <li><a href="home.php">Home</a></li>
                <li><a href="Five types of fitness training.php">Five types of fitness training-Flexibility Training</a>
                    <ul>
                        <li><a href="Dynamic.php">Dynamic Strength-training</a></li>
                        <li><a href="Static.php">Static Strength-training</a></li>
                        <li><a href="Aerobic.php">Aerobic Training</a></li>
                        <li><a href="Circuit.php">Circuit Training</a></li>
                   
            </ul>
        </li>
        <li><a href="About us.php">About us</a></li>
        <li><a href="Contact us.php">Contact us</a></li>
    </ul>
</nav>
<div>
    <main>
        <section>
            <h1>Static Strength-training</h1>
        </section>
        <p>
            Static strength-training is also considered an anaerobic exercise, and is also known as isometric exercise. This type of exercise helps you to maintain muscle strength and tone. 
            Isometrics involve contracting a muscle without moving any joints. Isometric exercises come in two types: submaximal and maximal. 
            Submaximal exercises involve contracting your muscles with less than your maximum strength, such as holding a dumbbell steady with your arm fully extended outward. 
            Maximal exercises involve contracting your muscles with all your strength, such as pushing against an immovable object.
        </p>
        <section>
            <h2>The Benefits of Isometric Exercise</h2>
        </section>
        <p>
            When it comes to resistance training, most individuals look to enhance their muscle tone, size or strength – or a combination of these three variables. The effect of isometric exercise on the target muscle itself will be considered during the research section below.

            There are numerous further benefits which isometric exercise offers over its popular movement orientated alternatives. For starters, isometric exercise can be performed almost anywhere and doesn’t rely heavily on expensive equipment or weights to be successful.
            <img src="apps-health-fitness1.jpg">
            Furthermore, through isometric exercise, you are able to create a contraction within the target muscle without placing too much stress and strain through the joint in question. 
            This is a strong criticism of traditional weight lifting, with both repetitive concentric and eccentric actions being associated with traumatic and overuse type injuries. 
            This obstacle is removed with isometric exercise as there is no movement involved – consequently it is a great tool to use with both novices and those recovering from injuries.

            Last but by no means least, the difficulty of isometric exercise can be easily varied to meet the needs and requirements of the individual performing the exercise. 
            Often, this is simply done by extending the time period for which you are required to hold the isometric contraction; it’s simply a test of time, you against the clock.
        </p>
    </main>

</div>
</body>
</html>

