<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="Homestyle.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <nav>
            <ul>
                <li><a href="home.php">Home</a></li>
                <li><a href="Five types of fitness training.php">Five types of fitness training-Flexibility Training</a>
                    <ul>
                        <li><a href="Dynamic.php">Dynamic Strength-training</a></li>
                        <li><a href="Static.php">Static Strength-training</a> </li>
                        <li><a href="Aerobic.php">Aerobic Training</a></li>
                        <li><a href="Circuit.php">Circuit Training</a></li>                    
                    </ul>
                </li>
                <li><a href="About us.php">About us</a></li>
                <li><a href="Contact us.php">Contact us</a></li>
            </ul>
        </nav>
        <div>
            <audio controls>
                <source src="Grapes - I dunno.ogg" type="audio/ogg">
                <source src="Grapes - I dunno.mp3" type="audio/mpeg">
                Your browser does not support the audio element.
            </audio>
            <main>
                <section>
                    <h1 style="color:blue;font-size:300%">Welcome</h1>

                </section>
                <p class="main">
                    For years everyone has known that regular exercise along with good nutrition is good for their health. 
                    The trick is how to build sound exercise habits and a balanced diet into your busy schedule. 
                    The stress of modern times mandates that you develop and maintain a fit, trim and fully functioning body.
                    Being active and physically fit heightens your self-expression and self-esteem.
                    Research polls indicate that people today are becoming more health centered. 
                    As a result, people are becoming more interested in making fitness exercise an integral part of their life-style. 
                </p>
                <section>
                    <h2 style="color:blue;font-size:300%">Why is the Physical Activity & Fitness Important?</h2>
                </section>
                <p>
                    Would you like to:
                <ol>
                    <li>Decrease your risk of disease?</li>
                    <li>Feel better physically and mentally?
                    <li>Help avoid injuries?</li>
                </ol>
                Regular physical activity will help you do these things. Physical activity is essential to prevent and reduce risks of many diseases and improve physical and mental health. 
                It can even help you live longer—research from the American Journal of Preventative Medicine indicates that regular exercise can add up to five years to your life.
                Physical activity also keeps you in shape so you can enjoy leisure activities and safely perform work and home chores. It offers great mental and social benefits as well. 
                The Lancet released a series of studies that attribute positive outcomes to physical activity, including “a sense of purpose and value, a better quality of life, improved sleep, and reduced stress, as well as stronger relationships and social connectedness.”
                On the other hand, lack of physical activity is associated with increased risks of:
                Anxiety, stress, and feelings of depression
                Developing many preventable conditions, such as high blood pressure, coronary heart diseases, diabetes, osteoporosis, colon cancer, and obesity
                Dying prematurely
                The authors of the Lancet studies even suggest that the sedentary lifestyle so common in our culture is more deadly than smoking. 
                They also believe that 6-10% of the world’s non-communicable diseases (such as heart disease, diabetes, and certain kinds of cancer) are caused by physical inactivity.
                <ul style="list-style-type:upper-roman">
                    <li style="color:green;font-size:150%">12 important reasons to be physically active</li>
                    <p>
                    <ul>
                        <li>Be healthier</li>

                        <li>Increase your chances of living longer</li>

                        <li>Feel better about yourself</li>

                        <li>Reduce the chance of becoming depressed</li>

                        <li>Sleep better at night</li>

                        <li>Look good</li>

                        <li>Be in shape</li>

                        <li>Get around better</li>

                        <li>Have stronger muscles and bones</li>

                        <li>Achieve or maintain a healthy weight</li>

                        <li>Be with friends or meet new people</li>

                        <li>Have fun</li>
                    </ul>
                    </p>
                    <li style="color:green;font-size:150%">Physical activity reduces risk for eight conditions</li>
                    <p>
                        According to the Centers for Disease Control, exercise can reduce your risk of:
                    <ul>
                        <li>Heart disease</li>
                        <li>Stroke</li>
                        <li>High blood pressure</li>
                        <li>Type 2 diabetes</li>
                        <li>Obesity</li>
                        <li>Depression</li>
                        <li>Breast and colon cancer</li>
                        <li>Osteoporosis</li>
                    </ul>
                    </p>
                </ul>
                <img src="HealthFitness.jpg" style="width:1100px;length:300px" >
                </p>
            </main>
        </div>
    </body>
</html>
