function validateData()
{

    if (document.theForm.name.value.length < 2)
    {
        document.getElementById("showErrorMessage1").innerHTML = "TOO SHORT: - should be at least 2 characters in length.";
        document.theForm.name.select();
        return false;
    }
    document.getElementById("showErrorMessage1").innerHTML = "";




    if (document.theForm.name.value.charAt(0) < "A" || document.theForm.name.value.charAt(0) > "Z")
    {
        alert("The name must start with an UPPERCASE letter.");
        document.getElementById("showErrorMessage1").innerHTML = "Must start with uppercase letter";
        document.theForm.name.select();
        return false;
    }
    if (document.theForm.address.value.length < 10)
    {
        document.getElementById("showErrorMessage2").innerHTML = "TOO SHORT: - should be at least 10 characters in length.";
        document.theForm.address.select();
        return false;
    }
    document.getElementById("showErrorMessage2").innerHTML = "";
    if (document.theForm.mobile.value === "")
    {
        alert("The mobile is empty");
        document.theForm.mobile.select();
        return false;
    }
    document.getElementById("showErrorMessage5").innerHTML = "";
    if (document.theForm.comment.value === "")
    {
        alert("The comment is empty");
        document.theForm.comment.select();
        return false;
    }
    document.getElementById("showErrorMessage6").innerHTML = "";
    if (document.theForm.email.value === "")
    {
        alert("The email is empty");
        document.theForm.email.select();
        return false;
    }
    document.getElementById("showErrorMessage4").innerHTML = "";












    if (document.theForm.type.selectedIndex === 0)
    {
        alert("A type was not picked from the selection list.");
        document.theForm.type.focus();
        return false;
    }




    // check if NO preference radio button checked
    var radioNotChecked = true;
    for (var j = 0; j < document.theForm.preference.length; j++)
    {
        if (document.theForm.preference[j].checked)
        {
            radioNotChecked = false;
        }
    }

    if (radioNotChecked)
    {
        alert("Contact preference was not picked from the RADIO BUTTON set.");
        document.theForm.preference[0].focus();
        return false;
    }





// No validation errors
    writeLocalStorage();
    return true;

}

function writeLocalStorage()
{
    // WRITE TO LOCAL STORAGE, INSERTING "N/A" FOR 
    //		any NON-REQUIRED TEXTBOXES IF THEY ARE EMPTY

    localStorage.fullName = document.theForm.name.value;

    localStorage.address = document.theForm.address.value;

    if (document.theForm.age.value.length === 0)
    {
        localStorage.age = "N/A";
    }
    else
    {
        localStorage.age = document.theForm.age.value;
    }
    localStorage.typeName = document.theForm.type.value;
    localStorage.emailAddress = document.theForm.email.value;
    localStorage.rating = document.theForm.rate.value;
    localStorage.date = document.theForm.date.value;
    
    localStorage.comment = document.theForm.comment.value;
   


}
